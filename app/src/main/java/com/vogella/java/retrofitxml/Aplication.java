package com.vogella.java.retrofitxml;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Aplication extends AppCompatActivity {

    public static void main(String[] args) {
        Controller ctrl = new Controller();
        ctrl.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
